import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';


void main() {
  runApp(MaterialApp(
    title: 'Navigation Basics',
    initialRoute: '/',
    routes: {
      // When navigating to the "/" route, build the FirstScreen widget.
      '/': (context) => FirstScreen(),
      // When navigating to the "/second" route, build the SecondScreen widget.
      '/second': (context) => SecondScreen(),
    },
  ));
}

class FirstScreen extends StatefulWidget {
  @override
  _MyFirstScreen createState() => _MyFirstScreen();
}
class _MyFirstScreen extends State<FirstScreen> with TickerProviderStateMixin {

  AnimationController _controller;
  Tween<double> _tweenSize = Tween(begin: 0.75, end: 2);

  initState() {
    super.initState();
    _controller = AnimationController(
        duration: const Duration(milliseconds: 700), vsync: this);
    _controller.repeat(reverse: true);

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          RaisedButton(
            padding: EdgeInsets.fromLTRB(160, 500, 100, 0),
            child: Icon(Icons.keyboard_arrow_right,color: Colors.blueGrey,size: 50,),
            onPressed: (){
              Navigator.pushNamed(context, '/second');

            },
            color: Colors.black,
          ),
          Align(
            child: ScaleTransition(
              scale: _tweenSize.animate(CurvedAnimation(parent: _controller, curve: Curves.elasticOut)),
              child: SizedBox(
                height: 100,
                width: 100,
                child: CircleAvatar(backgroundImage: AssetImage('assests/images/welcome.jpg')
                ),

              ),
            ),
          ),

          ]
          )
      );
  }
}

class SecondScreen extends StatefulWidget {
  @override
  _MySecondScreen createState() => _MySecondScreen();
}
class _MySecondScreen extends State<SecondScreen> with  TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..repeat(reverse: true);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double smallLogo = 150;
    final double bigLogo = 300;

    return LayoutBuilder(
      builder: (context, constraints) {
        final Size biggest = constraints.biggest;
        return Stack(
          children: [
            new Column(
              children: <Widget>[
                Padding(padding: EdgeInsets.fromLTRB(30, 60, 30, 0),),
                Icon(Icons.videogame_asset, color: Colors.blueGrey, size: 50,),
                Text(
                  "The sport of soccer (called football in most of the world) is considered to be the world's most popular sport."
                      "In soccer there are two teams of eleven players. Soccer is played on a large grass field with a goal at each end. "
                      "The object of the game is to get the soccer ball into the opposing team's goal. "
                      "The key to soccer is that, with the exception of the goalie, players cannot touch the ball with their hands, "
                      "they can only kick, knee, or head the ball to advance it or score a goal.",
                  style: TextStyle(fontSize: 25,
                      fontFamily: 'Sansita',
                      color: Colors.blueGrey), textAlign: TextAlign.justify,),
                Padding(padding: EdgeInsets.fromLTRB(0, 20, 0, 0),),
                RaisedButton(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Icon(Icons.keyboard_arrow_left, color: Colors.blueGrey,
                    size: 50,),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  color: Colors.black,
                ),

              ],

            ),
            PositionedTransition(
              rect: RelativeRectTween(
                begin: RelativeRect.fromSize(
                    Rect.fromLTWH(0, 0, smallLogo, smallLogo), biggest),
                end: RelativeRect.fromSize(
                    Rect.fromLTWH(biggest.width - bigLogo,
                        biggest.height - bigLogo, bigLogo, bigLogo),
                    biggest),
              ).animate(CurvedAnimation(
                parent: _controller,
                curve: Curves.elasticInOut,
              )),
              child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: new Image.asset('assests/images/soccer ball.png')),
            ),
          ],
        );
      },
    );
  }

  }
